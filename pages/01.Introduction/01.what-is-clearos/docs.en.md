---
title: ClearOS
visible: true
---

Simply put, ClearOS is an open platform for deploying Server, Network and Gateway functionality.

If you're into analogies, think of ClearOS, newly installed on any hardware, as an Android phone that's just been un-boxed.  ClearOS has certain basic functionalities just as a smartphone does.  To expand upon that functionality, an admin/owner would use the ClearOS Marketplace (Google Play Store) to install individual apps.

ClearOS is based on the [CentOS Project](https://www.centos.org).  Users (and developers) of ClearOS benefit tremendously from the dedicated teams upstream that provide development, bug reporting, feature enhancements, packaging, translations and other resources to provide a stable and secure Operating System to act as the foundation to ClearOS.
## Adding Value
ClearOS adds value by:
1. Intelligently integrating open source projects, products and services
2. Developing new and innovative custom apps and services
3. Providing a common framework for administrative management
4. Simplifying the installation process
5. Adding a Marketplace for developers of both free and paid software to showcase their apps

## Types of Apps
Most, if not all, of ClearOS apps can be placed in one of three categories:
1. Administrative "Wrapper"
2. Full stack app
3. Custom Development

### Administrative "Wrapper"
The majority of apps currently in the Marketplace consists of wrappers around established FOSS projects.  Examples include:
* Mail SMTP (Postfix)
* Mail IMAP/POP (Cyrus)
* Incoming Firewall (iptables)
* Softare RAID Manager (mdadm)
* Intrusion Detection (Snort)

Along with simplying the integration process and integrating the configuration with other apps, the administrative wrapper exposes some (but certainly  not all) configuration for the administrator to tweak the behavior.

### Full Stack
A number of apps from FOSS projects and software vendors (ISVs) come with their own user interfaces.  In this case, the ClearOS app will facilate the installation and possibly provide some administration, but essentially, users and admins will jump from ClearOS's Webconfig to the dedicated full stack app.  Examples of this type of app include:
* Plex Media Server (Plex)
* Kopano Messaging and Collaboration (Kopano)
* ownCloud Home and Business (ownCloud)

### Custom
The final class of apps consist of software written specifically for ClearOS.  Examples include:
* Mail Archive
* Events Framework
* Users and Groups
* MultiWAN
* Marketplace
