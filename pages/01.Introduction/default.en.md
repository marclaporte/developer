---
title: Introduction
published: true
---

Welcome to ClearOS Developer Documentation.

ClearOS is an Open-Source, Linux-based OS platform that is designed from the ground up to be extensible. ClearOS provides any combination of network, gateway and server functionality to small/medium sized business, non-profit organizations, education/government and distributed enterprise.

Supporting best-in-class open source and 3rd party applications through one secure, intuitive web-based interface, ClearOS is about choice - giving administrators the ability to run services on-premise or in the cloud, or both - "Hybrid Technology".

! This documentation is for developers. If you are looking for more general documentation like installation or administration guides, please go [here](https://www.clearos.com/resources/documentation/documentation-overview).

The developer documentation provided here is continually kept up-to-date based on the latest code.  If a feature or change has been made in a more recent version, you will see a version tag to indicate what version it was introduced.

Before you get scared away by the word developer you should know that you don't need to code software to contribute! You can:

* Help with translations
* Submit, diagnose and verify bugs
* Create new themes
* Develop new apps
* Contribute with packaging RPMs and releases
* Test code produced by developers and make suggestions
* Help design core parts of the architecture
