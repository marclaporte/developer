---
title: 'Development Environment'
visible: true
---

Whether you are translating, creating or patching apps, or building RPM packages, you will want to create a ClearOS development environment!

## Installing the Development Tools
Before getting underway, the first thing you need to do is install a ClearOS system. Thanks to the age of virtual machines, you no longer need dedicated hardware. Go ahead and install ClearOS on your favorite virtual machine (or dedicated hardware) and get it up and running.

Once you have the system up and running, login as 'root' through SSH instead of at the console. This will give you lots of advantages including the ability to scroll back easier and copy and paste information from this guide.

If you're running a Linux workstation/laptop, well, we'll assume you know how to access a shell.

For Windows workstations accessing the 'dev' environment, you can use [PuTTY](https://www.chiark.greenend.org.uk/~sgtatham/putty/latest.html). For Mac, use the Terminal program located in the Utilities folder of your Applications. We will use root for now but will use a named user account later. Since you are maintaining a development environment, we recommend you keep the ClearOS Developer and EPEL software repositories enabled since most of the development tools live there:

    yum-config-manager --enable clearos-developer,clearos-centos,clearos-centos-updates,clearos-epel
    
You can then install a basic set of development tools with the following commands:

    yum upgrade
    yum install clearos-devel app-devel

Don't forget to install the text editor of your choice, for example:

    yum install vim-enhanced
    
## Create the Environment
Now that all the development packages are installed, it is time to prep the following:
* User Account Creation
* Environment Variables
* RPM Packaging Directories
* Permissions and Sudoers
* Development/Sandboxed Webconfig

You can find the technical details of each of the above steps in the [Section Appendix](/getting-started/section-appendix) - the following command performs all the necessary steps:

    clearos setup
    
You will need to specify a username, password and default text editor.

!!! In the clearos setup command, please specify a new user account instead of using an existing account in LDAP. In development mode, LDAP may not always be available so it is not advisable to use an LDAP user.