---
title: 'Getting Started'
visible: true
---

In order to contribute to ClearOS as a developer, it helps to understand how ClearOS is built and maintained, what constitutes an app and what development languages you might be working.

It's important to note that being a 'jack of all trades' is not a requirement.  For example, if you're a web developer/designer, you can use your knowledge in HTML/CSS to improve or create new themes for Webconfig.  Similarly, if you are a PHP developer, you'll feel right at home with ClearOS's framework and can utilize a collection of widgets to abstract coding of the 'look and feel' and focus on functionality.  An additional benefit of using common widgets is a unified user experience.
