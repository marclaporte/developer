---
title: 'SSH Keys'
visible: true
---

Creating an RSA public/private key pair will save you a lot of time and allow you to securely access code repositories to checkout and commit (both your own, and one's in which you are added to as a developer/contributor).

! You can create a RSA key pair on the ClearOS development server or on your main workstation/laptop where you'll access your ClearOS development environment from.

## Generating Your SSH Key
On a Mac or Linux client, you generate an SSH key using the Terminal application.  In Windows, you'll use the PuTTY application.

### Terminal for Mac

1. In Finder, choose Utilities from the Applications folder
2. Find Terminal in the Utilities list
3. Open Terminal
4. Type <code>ssh-keygen</code> and follow the prompts, remembering where you saved both the private and public key pairs

### Terminal for Linux

1. Click on Menu
2. Click on Administration
3. Open terminal
4. Type <code>ssh-keygen</code> and follow the prompts, remembering where you saved both the private and public key pairs
 
### PuTTY for Windows
PuTTY is a free implementation of SSH and Telnet for Windows.

1. If required, download and install [PuTTY MSI Installer](https://www.chiark.greenend.org.uk/~sgtatham/putty/latest.html).
2. Launch PuTTY, and click the Generate button to create a key pair
3. Save the public and private keys by clicking the Save public key and Save private key buttons
4. From the **Public key for pasting into OpenSSH authorized_keys** file field at the top of the window, copy all the text starting with **ssh-rsa**
