---
title: 'Section Appendix'
visible: true
slug: section-appendix
---

## User Account Creation
A standard user account is needed – no need for root, no need for LDAP. On a ClearOS system, the user ID range from 500-999 is reserved for non-LDAP user accounts, while standard LDAP user accounts being at 1000. If you are curious, more information on ClearOS ID ranges is here

The webconfig server also needs read access to the user directory, hence the need for the chmod the /home/username directory.

## Environment Variables
Three environment variables are used in the ClearOS development environment:

### CLEAROS_BOOTSTRAP
The CLEAROS_BOOTSTRAP environment variable is used by the ClearOS app framework to specify an alternate root to /usr/clearos. You only need this environment variable if you are contributing to the underlying framework (CodeIgniter).

### CLEAROS_CONFIG
The CLEAROS_CONFIG environment variable sets the location of the ClearOS app configuration file (more on this later). By default, the configuration file is set to ~/.clearos.
### EDITOR
The EDITOR environment variable is used by SVN, CVS and other tools.

## RPM Packaging
Once you get to the RPM packaging stage, a special RPM dist tag is traditionally used to distinguish your RPM packages from others. You have probably seen these tags before: el6, centos6, fc19, v6, etc. When you are done with your first ClearOS RPM build, you will see the dist tag in the RPM filename, for example myapp-1.0-1.mydist.noarch.rpm.

By default, your username is used for the dist tag, but you can change the parameter by editing the ~/.rpmmacros file.

For app development, you don't need to know anything about creating RPMs, but you should know about where packages are built:

| Directory | Description |
| --------- | ----------- |
| BUILD	| Scratch area for the build process |
| BUILDROOT	| Scratch area for the installation |
| RPMS/* | Where the build puts the RPMs |
| SOURCES | Source code and patches needed to build an RPM |
| SPECS | RPM spec files |
| SRPMS | Where the build puts the source RPMs |

## Permissions and Sudoers
You will sometimes want to run scripts with the equivalent privileges as “webconfig”. An entry in the /etc/sudoers file grants your development account the same privileges as the ClearOS webconfig engine. Specifically, the following line was added to /etc/sudoers:

    //username// ALL=NOPASSWD: CC
    
## Development Webconfig Port - 1501
To access your development sandbox via a web browser, an alternate webconfig configlet configuration file pointing to port 1501 is created. You can find the file in /usr/clearos/sandbox/etc/httpd/conf.d/devel.conf and it will look something like the following.

    Listen 1501


    # Document root
    DocumentRoot /usr/clearos/framework/htdocs
    SetEnv CLEAROS_CONFIG /home/devuser/.clearos

    # For framework development only
    # DocumentRoot /home/devuser/clearos/webconfig/framework/trunk/htdocs
    # SetEnv CLEAROS_BOOTSTRAP /home/devuser/clearos/webconfig/framework/trunk/shared

    # Enable SSL
    SSLEngine on
    SSLProtocol all -SSLv2
    SSLCipherSuite ALL:!ADH:!EXPORT:!SSLv2:RC4+RSA:+HIGH:+MEDIUM:+LOW
    SSLCertificateFile /usr/clearos/sandbox/etc/httpd/conf/server.crt
    SSLCertificateKeyFile /usr/clearos/sandbox/etc/httpd/conf/server.key

    # Rewrites
    RewriteEngine on
    RewriteCond %{REQUEST_METHOD} ^(TRACE|TRACK)
    RewriteRule .* - [F]
    RewriteRule ^/app/(.*)$ /app/index.php/$1 [L]

    # Aliases
    Alias /cache/ /var/clearos/framework/cache/
    Alias /approot/ /usr/clearos/apps/
    Alias /themes/ /usr/clearos/themes/